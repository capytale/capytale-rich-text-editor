import React, {
  FunctionComponent,
  forwardRef,
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState,
} from "react";
import CapytaleEditor, { CapytaleEditorProps } from "./CapytaleEditor";
import {
  EditorContext,
  StateGetter,
  useEditorContext,
} from "./contexts/EditorContext";

type CapytaleRichEditorHook = [
  FunctionComponent<CapytaleEditorProps>,
  StateGetter,
  boolean | undefined,
];

export default function useCapytaleRichTextEditor(): CapytaleRichEditorHook {
  const subEditorRef = useRef<SubEditorHandle>(null);
  const getState = useCallback(
    (needJson, needHtml) => subEditorRef.current.getState(needJson, needHtml),
    []
  );
  const [canSave, setCanSave] = useState<boolean>(false);

  const Editor = useMemo(
    () => (props: CapytaleEditorProps) => (
      <EditorContext>
        <SubEditor
          ref={subEditorRef}
          setCanSave={(canSave) => {
            setCanSave(canSave);
          }}
          {...props}
        />
      </EditorContext>
    ),
    []
  );

  return [Editor, getState, canSave];
}

type SubEditorProps = CapytaleEditorProps & {
  setCanSave: (canSave: boolean) => void;
};

type SubEditorHandle = {
  getState: StateGetter;
};

const SubEditor = forwardRef<SubEditorHandle, SubEditorProps>(
  ({ setCanSave, ...props }, ref) => {
    const editorContext = useEditorContext();
    useImperativeHandle(ref, () => ({
      getState: editorContext.getState,
    }));
    useEffect(() => {
      setCanSave(editorContext.canSave);
    }, [editorContext.canSave]);

    return <CapytaleEditor {...props} />;
  }
);
