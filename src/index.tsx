import CapytaleEditor from "./CapytaleEditor";
import { EditorContext } from "./contexts/EditorContext";
import { useEditorContext } from "./contexts/EditorContext";
import useCapytaleRichTextEditor from "./useEditor";
import { InitialEditorStateType } from "@lexical/react/LexicalComposer";

export {
  CapytaleEditor as CapytaleRichTextEditor,
  EditorContext as CapytaleRichTextEditorContext,
  useEditorContext as useCapytaleRichTextEditorContext,
  useCapytaleRichTextEditor,
};

export type { InitialEditorStateType };
